<div class="content p-4">
        	
                <h2 class="mb-4">Import Absensi Harian</h2>

                <div class="card mb-4">
                    <div class="card-body">
                    <form method="post" action="" enctype="multipart/form-data">
				<a href="format_absen_harian.xlsx" class="btn btn-info">
					<span class="glyphicon glyphicon-download"></span>
					Download Format
				</a><br><br>
				
				<!-- 
				-- Buat sebuah input type file
				-- class pull-left berfungsi agar file input berada di sebelah kiri
				-->
				<input type="file" name="file" class="pull-left">
				
				<button type="submit" name="preview" class="btn btn-success btn-sm">
					<span class="glyphicon glyphicon-eye-open"></span> Preview
				</button>
			</form>


            <?php
			// Jika user telah mengklik tombol Preview
			if(isset($_POST['preview'])){
				//$ip = ; // Ambil IP Address dari User
				$nama_file_baru = 'data.xlsx';
				
				// Cek apakah terdapat file data.xlsx pada folder tmp
				if(is_file('tmp/'.$nama_file_baru)) // Jika file tersebut ada
					unlink('tmp/'.$nama_file_baru); // Hapus file tersebut
				
				$tipe_file = $_FILES['file']['type']; // Ambil tipe file yang akan diupload
				$tmp_file = $_FILES['file']['tmp_name'];
				
				// Cek apakah file yang diupload adalah file Excel 2007 (.xlsx)
				if($tipe_file == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
					// Upload file yang dipilih ke folder tmp
					// dan rename file tersebut menjadi data{ip_address}.xlsx
					// {ip_address} diganti jadi ip address user yang ada di variabel $ip
					// Contoh nama file setelah di rename : data127.0.0.1.xlsx
					move_uploaded_file($tmp_file, 'tmp/'.$nama_file_baru);
					
					// Load librari PHPExcel nya
					require_once 'PHPExcel/PHPExcel.php';
					
					$excelreader = new PHPExcel_Reader_Excel2007();
					$loadexcel = $excelreader->load('tmp/'.$nama_file_baru); // Load file yang tadi diupload ke folder tmp
					$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
					
					// Buat sebuah tag form untuk proses import data ke database
					echo "<form method='post' action='content/aksi_import.php'>";
					
					// Buat sebuah div untuk alert validasi kosong
					echo "<div class='alert alert-danger' id='kosong'>
					Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
					</div>";
					
					echo "<table class='table table-bordered'>
					<tr>
						<th colspan='13' class='text-center'>Preview Data</th>
					</tr>
					<tr>
						<th>No</th>
						<th>Hari</th>
						<th>Tanggal</th>
						<th>Pin</th>
						<th>Nip</th>
						<th>Nama</th>
						<th>Unit</th>
						<th>Sub Unit</th>
						<th>Shift</th>
						<th>Masuk</th>
						<th>Keluar</th>
						<th>Lokasi Masuk</th>
						<th>Lokasi Pulang</th>
					</tr>";
					
					$numrow = 1;
					$kosong = 0;
					$no = 1;
					$data_array = array();
					foreach($sheet as $row){ // Lakukan perulangan dari data yang ada di excel
						// Ambil data pada excel sesuai Kolom
						$hari = $row['B']; // Ambil data hari
						$tanggal = $row['C']; // Ambil data tanggal
						$pin = $row['D']; // Ambil data pin 
						$nip = $row['E']; // Ambil data nip
						$nama = $row['F']; // Ambil data nama
						$unit = $row['G'];
						$subunit = $row['H'];
						$shift = $row['I'];
						$masuk = $row['J'];
						$keluar= $row['K'];
						$lokasi_masuk = $row['L'];
						$lokasi_keluar = $row['M'];

						// Cek jika semua data tidak diisi
						if(empty($hari) && empty($tanggal) && empty($pin) && 
						empty($nip) && empty($nama))
							continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)
						
						// Cek $numrow apakah lebih dari 1
						// Artinya karena baris pertama adalah nama-nama kolom
						// Jadi dilewat saja, tidak usah diimport
						if($numrow > 1){
							// Validasi apakah semua data telah diisi
							$hari_td = ( ! empty($hari))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
							$tanggal_td = ( ! empty($tanggal))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
							$pin_td = ( ! empty($pin))? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
							$nip_td = ( ! empty($nip))? "" : " style='background: #E07171;'"; // Jika Telepon kosong, beri warna merah
							$nama_td = ( ! empty($nama))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
							$unit_td = ( ! empty($unit))? "" : " style='background: #E07171;'";
							$subunit_td = ( ! empty($subunit))? "" : " style='background: #E07171;'";
							$shift_td = ( ! empty($shift))? "" : " style='background: #E07171;'";
							$masuk_td = ( ! empty($masuk))? "" : " style='background: #E07171;'";
							$keluar_td = ( ! empty($kelar))? "" : " style='background: #E07171;'";
							$lokasi_masuk_td = ( ! empty($lokasi_masuk))? "" : " style='background: #E07171;'";
							$lokasi_keluar_td = ( ! empty($lokasi_keluar))? "" : " style='background: #E07171;'";
							// Jika salah satu data ada yang kosong
							if(empty($hari_td) or 
								empty($tanggal_td) or 
								empty($pin_td) or 
								empty($nip_td) or empty($nama_td)){
								$kosong++; // Tambah 1 variabel $kosong
							}

							
							$nama = mysql_real_escape_string($nama);
							

								$data_array[] = "(
									'".$tanggal."', 
									'".$pin."',
									'".$nip."',
									'".$nama."',
									'".$unit."',
									'".$subunit."',
									'".$shift."',
									'".$masuk."',
									'".$keluar."',
									'".$lokasi_masuk."',
									'".$lokasi_keluar."'
									)"; 


							echo "<tr>";
							echo "<td>".$no."</td>";
							echo "<td".$hari_td.">".$hari."</td>";
							echo "<td".$tanggal_td.">".$tanggal."</td>";
							echo "<td".$pin_td.">".$pin."</td>";
							echo "<td".$nip_td.">".$nip."</td>";
							echo "<td".$nama_td.">".$nama."</td>";
							echo "<td".$unit_td.">".$unit."</td>";
							echo "<td".$subunit_td.">".$subunit."</td>";
							echo "<td".$shift_td.">".$shift."</td>";
							echo "<td".$masuk_td.">".$masuk."</td>";
							echo "<td".$keluar_td.">".$keluar."</td>";
							echo "<td".$lokasi_masuk_td.">".$lokasi_masuk."</td>";
							echo "<td".$lokasi_keluar_td.">".$lokasi_keluar."</td>";
							echo "</tr>";
							$no++;
						}
						
						$numrow++; // Tambah 1 setiap kali looping
					}
					
					echo "</table>";
					
					// Cek apakah variabel kosong lebih dari 1
					// Jika lebih dari 1, berarti ada data yang masih kosong
					// if($kosong > 1){
					// ?>	
					// 	<script>
					// 	$(document).ready(function(){
					// 		// Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
					// 		$("#jumlah_kosong").html('<?php echo $kosong; ?>');
							
					// 		$("#kosong").show(); // Munculkan alert validasi kosong
					// 	});
					// 	</script>
					// <?php
                    // }
                    
                    // else{ // Jika semua data sudah diisi
					// 	echo "<hr>";
						
						// Buat sebuah tombol untuk mengimport data ke database
						echo "<button type='submit' name='import' class='btn btn-primary'><span class='glyphicon glyphicon-upload'></span> Import</button>";
					// }
					
					echo "</form>";
				}else{ // Jika file yang diupload bukan File Excel 2007 (.xlsx)
					// Munculkan pesan validasi
					echo "<div class='alert alert-danger'>
					Hanya File Excel 2007 (.xlsx) yang diperbolehkan
					</div>";
				}


				$strq = "";

				$strq = "INSERT INTO tbl_info (
					tanggal,pin,nip,nama,unitkerja,subunit,shift,masuk,keluar,lokasi_masuk,lokasi_keluar) VALUES";
				$strq .= implode(",", $data_array).";";
				// echo $strq;
				mysqli_query($connect, $strq);

			}
			?>


                    </div>
                </div>
        </div>