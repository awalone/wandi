<div class="content p-4">
        	
                <h2 class="mb-4">Absensi Harian</h2>

                <div class="card mb-4">
        <div class="card-header bg-white font-weight-bold">
            Daftar Absen Harian <strong>(10 Juli 2018)</strong>
            
        </div>
        <div class="card-body">
        <a href="?module=importharian" class="btn btn-primary btn-right">Import Absensi Harian</a><br /><br />
            <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" id="home-tab-2" data-toggle="tab" href="#home-2" role="tab" aria-controls="home" aria-selected="true"><strong>Tabel</strong></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab-2" data-toggle="tab" href="#profile-2" role="tab" aria-controls="profile" aria-selected="false"><strong>Grafik</strong></a>
                </li>
               
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade active show" id="home-2" role="tabpanel" aria-labelledby="home-tab-2">
                <div class="alert alert-success" role="alert">
                    5 Teratas Pada Hari Rabu (<strong>10 Juli 2018</strong>)
                </div>

                

                
                <?php 
                    // Buat query untuk menampilkan semua data siswa
					$sql = mysqli_query($connect, "SELECT *from tbl_info 
					where tanggal = '2018-07-06' AND unitkerja = 'AREA BAUBAU' AND masuk != '00:00:00' AND subunit = 'AREA BAU-BAU' order by masuk ASC LIMIT 0,5");
					$num_rows = mysqli_num_rows($sql);
                    $no = 1; // Untuk penomoran tabel, di awal set dengan 1
                    if ($num_rows > 0)
                    {
                        ?>
                        <table class="table mb-0">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Unit</th>
                                <th scope="col">Sub Unit</th>
                                <th scope="col">Shift</th>
                                <th scope="col">Jam Masuk</th>
                            </tr>
                            </thead>


                            <tbody>

                        <?php
                        while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                            echo "<tr>";
                            echo "<th scope='row'>".$no."</td>";
                            echo "<td>".$data['nama']."</td>";
                            echo "<td>".$data['unitkerja']."</td>";
                            echo "<td>".$data['subunit']."</td>";
                            echo "<td>".$data['shift']."</td>";
                            echo "<td>".$data['masuk']."</td>";
                            echo "</tr>";
                            
                            $no++; // Tambah 1 setiap kali looping
                        }
                        ?>
                        </tbody>
                        </table>
                        <?php
                    }

                    else 

                    {
                        ?>
                             <div class="alert alert-success" role="alert">
                                    <span style='color: red;'><strong>Absensi Belum Ada</strong>
                                </div>
                        <?php
                    }
					
                ?>
                
                

            <div class="row">&nbsp;<hr><p>&nbsp;</p></div>

            <div class="alert alert-danger" role="alert">
                    5 Terbawah Pada Hari Rabu (<strong>10 Juli 2018</strong>)
                </div>

                

                <table class="table mb-0">
                <thead class="thead-light">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Unit</th>
                    <th scope="col">Sub Unit</th>
                    <th scope="col">Shift</th>
                    <th scope="col">Jam Masuk</th>
                </tr>
                </thead>


                <tbody>
                <?php 
                    // Buat query untuk menampilkan semua data siswa
					$sql = mysqli_query($connect, "SELECT *from tbl_info 
					where tanggal = '2018-07-06' AND unitkerja = 'AREA BAUBAU' AND masuk != '00:00:00' AND subunit = 'AREA BAU-BAU' order by masuk DESC LIMIT 0,5");
					// $num_rows = mysqli_num_rows($sql);
					$no = 1; // Untuk penomoran tabel, di awal set dengan 1
					while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
						echo "<tr>";
						echo "<th scope='row'>".$no."</td>";
						echo "<td>".$data['nama']."</td>";
						echo "<td>".$data['unitkerja']."</td>";
						echo "<td>".$data['subunit']."</td>";
						echo "<td>".$data['shift']."</td>";
						echo "<td>".$data['masuk']."</td>";
						echo "</tr>";
						
						$no++; // Tambah 1 setiap kali looping
					}
                ?>
                
                </tbody>
            </table>




            <div class="row">&nbsp;<p>&nbsp;</p><p>&nbsp;</p></div>
                    <hr />
<div class="alert alert-danger" role="alert">
        Tidak Absen / Cuti / SPPD / Ijin (<strong>10 Juli 2018</strong>)
    </div>

    

    <table class="table mb-0">
    <thead class="thead-light">
    <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Unit</th>
        <th scope="col">Sub Unit</th>
        <th scope="col">Shift</th>
        <th scope="col">Jam Masuk</th>
    </tr>
    </thead>


    <tbody>
    <?php 
        // Buat query untuk menampilkan semua data siswa
        $sql = mysqli_query($connect, "SELECT *from tbl_info 
					where tanggal = '2018-07-06' AND unitkerja = 'AREA BAUBAU' AND masuk = '00:00:00'");
					// $num_rows = mysqli_num_rows($sql);
        $no = 1; // Untuk penomoran tabel, di awal set dengan 1
        while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
            echo "<tr>";
            echo "<th scope='row'>".$no."</td>";
            echo "<td>".$data['nama']."</td>";
            echo "<td>".$data['unitkerja']."</td>";
            echo "<td>".$data['subunit']."</td>";
            echo "<td>".$data['shift']."</td>";
            echo "<td>".$data['masuk']."</td>";
            echo "</tr>";
            
            $no++; // Tambah 1 setiap kali looping
        }
    ?>
    
    </tbody>
</table>
                    </div>
                <div class="tab-pane fade" id="profile-2" role="tabpanel" aria-labelledby="profile-tab-2">Etiam at posuere metus, in molestie libero. Cras eleifend turpis vel libero vehicula, interdum gravida lacus consectetur. Proin id sodales nunc, sit amet varius orci. Nulla malesuada consectetur ipsum a dapibus. Cras rhoncus justo ex, ac tincidunt mauris fermentum in. Vestibulum hendrerit metus pharetra arcu convallis dictum. Vestibulum leo erat, vehicula eu ex a, convallis faucibus nunc. Duis ultrices lacus quis vulputate iaculis. Ut magna sem, vulputate non tristique sit amet, egestas et purus. Mauris euismod sapien id pretium vulputate. In molestie, enim id finibus hendrerit, velit ante sodales nisi, vitae aliquam nulla tortor nec ligula.</div>
                
            </div>
        </div>
    </div>


               


                





                
            
        </div>