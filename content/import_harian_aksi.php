<?php

// Load file koneksi.php
include "../koneksi.php";

if(isset($_POST['import'])){ // Jika user mengklik tombol Import
	$nama_file_baru = '../data.xlsx';
	
	// Load librari PHPExcel nya
	require_once 'PHPExcel/PHPExcel.php';
	
	$excelreader = new PHPExcel_Reader_Excel2007();
	$loadexcel = $excelreader->load('../tmp/'.$nama_file_baru); // Load file excel yang tadi diupload ke folder tmp
	$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
	
	// Buat query Insert
	$query = "INSERT INTO tbl_info (
			tanggal,pin,nip,nama,unitkerja,subunit,shift,masuk,keluar,lokasi_masuk,lokasi_keluar) VALUES";
	


	$numrow = 1;
	foreach($sheet as $row){
		// Ambil data pada excel sesuai Kolom
		$hari = $row['B']; // Ambil data hari
		$tanggal = $row['C']; // Ambil data tanggal
		$pin = $row['D']; // Ambil data pin 
		$nip = $row['E']; // Ambil data nip
		$nama = $row['F']; // Ambil data nama
		$unit = $row['G'];
		$subunit = $row['H'];
		$shift = $row['I'];
		$masuk = $row['J'];
		$keluar= $row['K'];
		$lokasi_masuk = $row['L'];
		$lokasi_keluar = $row['M'];
		
		// Cek jika semua data tidak diisi
		if(empty($hari) && empty($tanggal) && empty($pin) && 
						empty($nip) && empty($nama))
							continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)
		
		// Cek $numrow apakah lebih dari 1
		// Artinya karena baris pertama adalah nama-nama kolom
		// Jadi dilewat saja, tidak usah diimport
		if($numrow > 1){
			// Tambahkan value yang akan di insert ke variabel $query
			$query .= "('".$tanggal."','".$pin."','".$nip."','".$nama."','".$unit."'
			,'".$subunit."','".$shift."','".$masuk."','".$keluar."','".$lokasi_masuk."','".$lokasi_keluar."'),";
		}
		
		$numrow++; // Tambah 1 setiap kali looping
	}
	
	// Kita hilangkan tanda koma di akhir query
	// sehingga kalau di echo $query nya akan sepert ini : (contoh ada 2 data siswa)
	
	$query = substr($query, 0, strlen($query) - 1).";";
	
	// Eksekusi $query
	mysqli_query($connect, $query);
}

header('location: index.php'); // Redirect ke halaman awal
?>
